package com.alessandrovdias.merchantsguidetogalaxy;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.alessandrovdias.merchantsguidetogalaxy.logic.GalacticProcessor;

/**
 * Main class, charged of starting the Merchants Guide Application by its main method
 * 
 * @author Alessandro
 *
 */
public class MerchantsGuideApplicationTest {
	
	@Rule
    public ExpectedException thrown = ExpectedException.none();
	
	private static final String RIGHT_INPUT_FILENAME = "inputs.txt";
	private static final String WRONG_INPUT_FILENAME = "wronginputs.txt";
	private static final String SOME_DATA_FOR_PROCESS_TEST = "somedata\\ntest\\r";
	
    private String testInputContent;
	
	@Test
	public void testInputFileRead() throws Exception {
		URL url = Thread.currentThread().getContextClassLoader().getResource(RIGHT_INPUT_FILENAME);
		testInputContent = new String(Files.readAllBytes(Paths.get(url.toURI())));
		String inputContent = MerchantsGuideApplication.inputFileRead(RIGHT_INPUT_FILENAME);
		assertThat(inputContent, is(equalTo(testInputContent)));
	}
	
	@Test
	public void testWrongInputFileRead(){
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("Invalid file name or URI");
		MerchantsGuideApplication.inputFileRead(WRONG_INPUT_FILENAME);
	}
	
	@Test
	public void testProcessInput(){
		MerchantsGuideApplication mga = new MerchantsGuideApplication();
		GalacticProcessor gpMock = mock(GalacticProcessor.class);
		mga.setGalacticParser(gpMock);
		MerchantsGuideApplication.processInputData(SOME_DATA_FOR_PROCESS_TEST);
		verify(gpMock).processData(SOME_DATA_FOR_PROCESS_TEST);		
	}

}
