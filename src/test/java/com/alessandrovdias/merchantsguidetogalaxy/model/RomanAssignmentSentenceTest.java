package com.alessandrovdias.merchantsguidetogalaxy.model;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class RomanAssignmentSentenceTest {
	@Rule
    public ExpectedException thrown = ExpectedException.none();
	
    RomanAssignmentSentence ras;

	@Before
	public void initObjects(){
		ras = new RomanAssignmentSentence();
	}

	@Test
	public void testProcessSuccessful() {
		Map<String, String> expectedAssignments = new HashMap<String, String>();
		expectedAssignments.put("glob", "I");
		assertThat(expectedAssignments, is(ras.process("glob is I")));
	}
	
	@Test
	public void testProcessSuccessfulTwo() {
		Map<String, String> expectedAssignments = new HashMap<String, String>();
		expectedAssignments.put("prok", "V");
		assertThat(expectedAssignments, is(ras.process("prok is V")));
	}
	
	@Test
	public void testProcessWrongValueAssigned() {
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("Invalid roman character in the data line: 1");
		ras.process("glob is 1");
	}
	
	@Test
	public void testProcessWrongValueAssignedTwo() {
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("Invalid roman character in the data line: glob");
		ras.process("glob is glob");
	}

	@Test
	public void testProcessWrongValueAssignedThree() {
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("Invalid roman character in the data line: MCVIIII");
		ras.process("glob is MCVIIII");
	}
	
	@Test
	public void testProcessWrongSentenceAssigned() {
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("Invalid sentence in the data line");
		ras.process("glob is glob is I");
	}

	@Test
	public void testProcessWrongSentenceAssignedTwo() {
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("Invalid sentence in the data line");
		ras.process("glob 1");
	}

}
