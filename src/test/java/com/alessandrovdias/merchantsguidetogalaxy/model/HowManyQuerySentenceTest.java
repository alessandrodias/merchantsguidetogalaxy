package com.alessandrovdias.merchantsguidetogalaxy.model;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class HowManyQuerySentenceTest {
	
	@Rule
    public ExpectedException thrown = ExpectedException.none();
	
	private HowManyQuerySentence hmqs;
	private PrintStream mockedPrintStream;

	
	@Before
	public void initObjects(){
		hmqs = new HowManyQuerySentence();
		Map<String, String> romanAssignments = new HashMap<String, String>();
		romanAssignments.put("glob", "I");
		romanAssignments.put("prok", "V");
		romanAssignments.put("pish", "X");		
		romanAssignments.put("tegj", "L");
		hmqs.setRomanAssignments(romanAssignments);
		
		Map<String, String> creditAssignments = new HashMap<String, String>();
		creditAssignments.put("Silver", "17");
		creditAssignments.put("Iron", "195.5");
		creditAssignments.put("Gold", "14450");
		hmqs.setCreditsAssignments(creditAssignments);
		
		mockedPrintStream = mock(PrintStream.class);
		
		System.setOut(mockedPrintStream);
	}
	
	@Test
	public void testSuccessfulHowManyQueryOne() {
		hmqs.process("how many Credits is glob prok Silver ?");
		verify(mockedPrintStream).println("glob prok Silver is 68 Credits");	
	}

	@Test
	public void testSuccessfulHowManyQueryTwo() {
		hmqs.process("how many Credits is glob prok Gold ?");
		verify(mockedPrintStream).println("glob prok Gold is 57800 Credits");	
	}

	@Test
	public void testSuccessfulHowManyQueryThree() {
		hmqs.process("how many Credits is glob prok Iron ?");
		verify(mockedPrintStream).println("glob prok Iron is 782 Credits");	
	}
	
	@Test
	public void testUnsuccessfulHowManyQueryOne() {
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("Invalid character in the data line");
		hmqs.process("how many Credits is 1 ?");		
	}
	
	@Test
	public void testUnsuccessfulHowManyQueryTwo() {
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("Invalid character in the data line");
		hmqs.process("how many is Gold glob ?");		
	}
	
	@Test
	public void testUnsuccessfulHowManyQueryThree() {
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("Invalid character in the data line");
		hmqs.process("how many is Golden glob ?");		
	}

	
}
