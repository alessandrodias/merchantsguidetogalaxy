package com.alessandrovdias.merchantsguidetogalaxy.model;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class CreditsAssignmentSentenceTest {

	@Rule
    public ExpectedException thrown = ExpectedException.none();
	
    CreditsAssignmentSentence cas;

	@Before
	public void initObjects(){
		cas = new CreditsAssignmentSentence();
	}
	
	@Test
	public void testProcessSuccessfulForSilver() {
		Map<String, String> expectedAssignments = new HashMap<String, String>();
		expectedAssignments.put("Silver", "17");
		Map<String, String> romanAssignments = new HashMap<String, String>();
		romanAssignments.put("glob", "I");
		cas.setRomanAssignments(romanAssignments);
		assertThat(expectedAssignments, is(cas.process("glob glob Silver is 34 Credits")));
	}
	
	@Test
	public void testProcessSuccessfulForIron() {
		Map<String, String> expectedAssignments = new HashMap<String, String>();
		expectedAssignments.put("Iron", "195.5");
		Map<String, String> romanAssignments = new HashMap<String, String>();
		romanAssignments.put("pish", "X");		
		cas.setRomanAssignments(romanAssignments);
		assertThat(expectedAssignments, is(cas.process("pish pish Iron is 3910 Credits")));
	}
	
	@Test
	public void testProcessSuccessfulForIronTwo() {
		Map<String, String> expectedAssignments = new HashMap<String, String>();
		expectedAssignments.put("Iron", "195.5");
		Map<String, String> romanAssignments = new HashMap<String, String>();
		romanAssignments.put("glob", "I");
		romanAssignments.put("prok", "V");
		cas.setRomanAssignments(romanAssignments);
		assertThat(expectedAssignments, is(cas.process("glob prok Iron is 782 Credits")));
	}

	@Test
	public void testProcessSuccessfulForGold() {
		Map<String, String> expectedAssignments = new HashMap<String, String>();
		expectedAssignments.put("Gold", "14450");
		Map<String, String> romanAssignments = new HashMap<String, String>();
		romanAssignments.put("glob", "I");
		romanAssignments.put("prok", "V");
		cas.setRomanAssignments(romanAssignments);
		assertThat(expectedAssignments, is(cas.process("glob prok Gold is 57800 Credits")));
	}

}
