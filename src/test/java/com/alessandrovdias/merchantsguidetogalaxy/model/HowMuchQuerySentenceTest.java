package com.alessandrovdias.merchantsguidetogalaxy.model;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class HowMuchQuerySentenceTest {
	
	@Rule
    public ExpectedException thrown = ExpectedException.none();
	
	private HowMuchQuerySentence hmqs;
	private PrintStream mockedPrintStream;
	
	@Before
	public void initObjects(){
		hmqs = new HowMuchQuerySentence();
		Map<String, String> romanAssignments = new HashMap<String, String>();
		romanAssignments.put("glob", "I");
		romanAssignments.put("pish", "X");		
		romanAssignments.put("tegj", "L");
		hmqs.setRomanAssignments(romanAssignments);

		mockedPrintStream = mock(PrintStream.class);
		
		System.setOut(mockedPrintStream);
	}

	@Test
	public void testSuccessfulHowMuchQuery() {
		hmqs.process("how much is pish tegj glob glob ?");
		verify(mockedPrintStream).println("pish tegj glob glob is 42");
	}
	
	@Test
	public void testSuccessfulHowMuchQueryTwo() {
		hmqs.process("how much is glob ?");
		verify(mockedPrintStream).println("glob is 1");
	}
	
	@Test
	public void testUnsuccessfulHowMuchQuery() {
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("Invalid character in the data line");
		hmqs.process("how much is 1 ?");		
	}
	
	@Test
	public void testUnsuccessfulHowMuchQueryTwo() {
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("Invalid character in the data line");
		hmqs.process("how much is Silver ?");		
	}
	
	@After
    public void tearDown() {
        System.setOut(null);
    }
}
