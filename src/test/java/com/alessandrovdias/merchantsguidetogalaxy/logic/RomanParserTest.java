package com.alessandrovdias.merchantsguidetogalaxy.logic;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class RomanParserTest {
	@Rule
	public ExpectedException thrown = ExpectedException.none();
	
	private RomanParser rp;

	@Before
	public void initObjects(){
		rp = new RomanParser();
	}

	@Test
	public void testParseValidValues() {
		assertEquals(new BigDecimal(1), rp.parseValue("I"));
		assertEquals(new BigDecimal(4), rp.parseValue("IV"));
		assertEquals(new BigDecimal(5), rp.parseValue("V"));
		assertEquals(new BigDecimal(7), rp.parseValue("VII"));
		assertEquals(new BigDecimal(44), rp.parseValue("XLIV"));
		assertEquals(new BigDecimal(59), rp.parseValue("LIX"));
		assertEquals(new BigDecimal(98), rp.parseValue("XCVIII"));
	}

	@Test
	public void testExtractInvalidValues() {
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("Invalid roman character in the data line: IIII");
		rp.parseValue("IIII");
	}

	@Test
	public void testExtractInvalidValuesTwo() {
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("Invalid roman character in the data line: XCIIX");
		rp.parseValue("XCIIX");
	}
	
	@Test
	public void testGetRightDecimalFromRomanAssignedPartOne(){
		Map<String, String> romanAssignments = new HashMap<String, String>();
		romanAssignments.put("glob", "I");
		assertEquals(new BigDecimal(1), rp.getDecimalFromRomanAssignedPart(new String[]{"glob"}, romanAssignments));
	}
	
	@Test
	public void testGetRightDecimalFromRomanAssignedPartTwo(){
		Map<String, String> romanAssignments = new HashMap<String, String>();
		romanAssignments.put("glob", "I");
		romanAssignments.put("pish", "X");
		assertEquals(new BigDecimal(9), rp.getDecimalFromRomanAssignedPart(new String[]{"glob","pish"}, romanAssignments));
	}
	
	@Test
	public void testGetDecimalFromWrongRomanAssignedPartOne(){
		Map<String, String> romanAssignments = new HashMap<String, String>();
		romanAssignments.put("glob", "I");
		romanAssignments.put("pish", "K");
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("Invalid roman character in the data line: IK");
		rp.getDecimalFromRomanAssignedPart(new String[]{"glob","pish"}, romanAssignments);
	}
	
	@Test
	public void testGetDecimalFromMissingRomanAssignedPart(){
		Map<String, String> romanAssignments = new HashMap<String, String>();
		romanAssignments.put("glob", "I");
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("Invalid character in the data line, could not find in romanAssignments");
		rp.getDecimalFromRomanAssignedPart(new String[]{"glob","pish"}, romanAssignments);
	}
}
