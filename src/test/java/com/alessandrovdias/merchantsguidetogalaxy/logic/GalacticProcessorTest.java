package com.alessandrovdias.merchantsguidetogalaxy.logic;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.alessandrovdias.merchantsguidetogalaxy.util.MyHashMap;

public class GalacticProcessorTest {

	@Rule
    public ExpectedException thrown = ExpectedException.none();
	
	GalacticProcessor gp;
	
	@Before
	public void initObjects(){
		gp = new GalacticProcessor();
	}

	@Test
	public void testProcessValidRomanAssignmentData() {
		List<String> expectedLines = Arrays.asList("glob is I", "prok is V");
		gp.processData("glob is I"+System.lineSeparator()+"prok is V");
        assertThat(gp.getLines(), is(expectedLines));
	}
	
	@Test
	public void testProcessInvalidAssignmentData() {
		gp.processData("glob is I I"+System.lineSeparator()+"prok is 99 Xredits !");
		assertTrue(gp.getRomanAssignments().isEmpty());
	}
	
	@Test
	public void testProcessValidHowMuchQueryData() {
		List<String> expectedLines = Arrays.asList("how much is pish tegj glob glob ?");
		Map<String, String> romanAssignments = new MyHashMap<String, String>();
		romanAssignments.put("glob", "I");
		romanAssignments.put("pish", "X");
		romanAssignments.put("tegj", "L");
		GalacticProcessor.setRomanAssignments(romanAssignments);
		gp.processData("how much is pish tegj glob glob ?");
        assertThat(gp.getLines(), is(expectedLines));
	}
	
	@Test
	public void testGetDecimalFromRomanAssignedPart(){
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("Cannot set an existing value to another assignment key");
		Map<String, String> romanAssignments = new MyHashMap<String, String>();
		romanAssignments.put("glob", "I");
		romanAssignments.put("pish", "I");
		GalacticProcessor.setRomanAssignments(romanAssignments);
		gp.processData("glob is I");
	}
	
}
