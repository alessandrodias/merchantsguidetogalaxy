package com.alessandrovdias.merchantsguidetogalaxy.logic;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.alessandrovdias.merchantsguidetogalaxy.model.CreditsAssignmentSentence;
import com.alessandrovdias.merchantsguidetogalaxy.model.HowManyQuerySentence;
import com.alessandrovdias.merchantsguidetogalaxy.model.HowMuchQuerySentence;
import com.alessandrovdias.merchantsguidetogalaxy.model.NoIdeaSentence;
import com.alessandrovdias.merchantsguidetogalaxy.model.RomanAssignmentSentence;

public class SentenceFactoryTest {
	
	private static final String TEST_ROMAN_DATA_LINE = "glob is I";
	private static final String TEST_CREDITS_DATA_LINE = "glob glob Silver is 34 Credits";
	private static final String TEST_HOW_MUCH_DATA_LINE = "how much is pish tegj glob glob ?";
	private static final String TEST_HOW_MANY_DATA_LINE = "how many Credits is glob prok Iron ?";
	private static final String TEST_INVALID_DATA_LINE = "how much wood could a woodchuck chuck if a woodchuck could chuck wood ?";
	private static final String TEST_ANOTHER_INVALID_DATA_LINE = "BANANA, banana, BaNaNa !";
	
	@Test
	public void testParseSentences() {
		assertEquals(RomanAssignmentSentence.class, SentenceFactory.parseSentence(TEST_ROMAN_DATA_LINE).getClass());
		assertEquals(CreditsAssignmentSentence.class, SentenceFactory.parseSentence(TEST_CREDITS_DATA_LINE).getClass());
		assertEquals(HowMuchQuerySentence.class, SentenceFactory.parseSentence(TEST_HOW_MUCH_DATA_LINE).getClass());
		assertEquals(HowManyQuerySentence.class, SentenceFactory.parseSentence(TEST_HOW_MANY_DATA_LINE).getClass());
		assertEquals(NoIdeaSentence.class, SentenceFactory.parseSentence(TEST_INVALID_DATA_LINE).getClass());
		assertEquals(NoIdeaSentence.class, SentenceFactory.parseSentence(TEST_ANOTHER_INVALID_DATA_LINE).getClass());
	}	
}
