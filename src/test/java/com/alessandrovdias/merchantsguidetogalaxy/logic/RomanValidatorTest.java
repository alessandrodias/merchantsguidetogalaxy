package com.alessandrovdias.merchantsguidetogalaxy.logic;

import static org.junit.Assert.assertTrue;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class RomanValidatorTest {
	@Rule
	public ExpectedException thrown = ExpectedException.none();
	
	@Test
	public void testValidateRomanInput() {
		assertTrue(RomanValidator.validateRomanInput("IV"));
	}	
	
	@Test
	public void testExtractInvalidValues() {
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("Invalid roman character in the data line: IIV");
		RomanValidator.validateRomanInput("IIV");
	}

	@Test
	public void testExtractInvalidValuesTwo() {
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("Invalid roman character in the data line: XCIIX");
		RomanValidator.validateRomanInput("XCIIX");
	}

}
