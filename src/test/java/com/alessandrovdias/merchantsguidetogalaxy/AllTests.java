package com.alessandrovdias.merchantsguidetogalaxy;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.alessandrovdias.merchantsguidetogalaxy.logic.GalacticProcessorTest;
import com.alessandrovdias.merchantsguidetogalaxy.logic.RomanParserTest;
import com.alessandrovdias.merchantsguidetogalaxy.logic.RomanValidatorTest;
import com.alessandrovdias.merchantsguidetogalaxy.logic.SentenceFactoryTest;
import com.alessandrovdias.merchantsguidetogalaxy.model.CreditsAssignmentSentenceTest;
import com.alessandrovdias.merchantsguidetogalaxy.model.HowManyQuerySentenceTest;
import com.alessandrovdias.merchantsguidetogalaxy.model.HowMuchQuerySentenceTest;
import com.alessandrovdias.merchantsguidetogalaxy.model.RomanAssignmentSentenceTest;

@RunWith(Suite.class)
@SuiteClasses({ 
	MerchantsGuideApplicationTest.class, 
	GalacticProcessorTest.class,
	SentenceFactoryTest.class,
	RomanAssignmentSentenceTest.class,
	RomanParserTest.class,
	RomanValidatorTest.class,
	CreditsAssignmentSentenceTest.class,
	HowMuchQuerySentenceTest.class,
	HowManyQuerySentenceTest.class})

public class AllTests {

}
