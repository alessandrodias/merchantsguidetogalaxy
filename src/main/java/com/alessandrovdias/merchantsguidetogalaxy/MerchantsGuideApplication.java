package com.alessandrovdias.merchantsguidetogalaxy;

import static java.nio.file.Files.readAllBytes;
import static java.nio.file.Paths.get;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

import com.alessandrovdias.merchantsguidetogalaxy.logic.GalacticProcessor;

/**
 * Main class, charged of starting the Merchants Guide Application by its main method
 * 
 * @author Alessandro
 *
 */
public class MerchantsGuideApplication {

	private static GalacticProcessor galacticProcessor;
	
	/**
	 * Method responsible for reading input lines from a text file, returning all file content in a String object
	 * 
	 * @param inputFilename name of the file with absolute file path + name
	 * @return String containing file contents
	 * @throws Exception 
	 * @throws URISyntaxException 
	 * @throws IOException 
	 */
	public static String inputFileRead(String inputFilename)  {
		try {
			URL url = Thread.currentThread().getContextClassLoader().getResource(inputFilename);
			return new String(readAllBytes(get(url.toURI())));
		} catch (IOException | URISyntaxException |  NullPointerException e){
			throw new IllegalArgumentException("Invalid file name or URI");
		}
	}

	public void setGalacticParser(GalacticProcessor galacticProcessor) {
		MerchantsGuideApplication.galacticProcessor = galacticProcessor;		
	}
	
	/**
	 * Method that signals to the galactic parser that it has data to parse
	 * 
	 * @param data
	 */
	public static void processInputData(String data) {
		galacticProcessor.processData(data);	
	}

	/**
	 * Main method, called with one parameter which contains the input data filename with extension 
	 * 
	 * @param args 
	 */
	public static void main(String[] args) {
		if (args.length==0 || args.length>1){
			System.out.println("Please inform input data filename with extension as the only parameter for MerchantsGuideApplication");
			System.exit(0);
		}
		try {
			galacticProcessor = new GalacticProcessor();
			processInputData(inputFileRead(args[0]));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
