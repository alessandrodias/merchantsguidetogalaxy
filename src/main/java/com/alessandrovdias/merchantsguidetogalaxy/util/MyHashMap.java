package com.alessandrovdias.merchantsguidetogalaxy.util;

import java.util.HashMap;
import java.util.Map;

/**
 * Utility class that implements a reverse HashMap to help guarantee that values from Map aren't repeated
 * 
 * @author Alessandro
 *
 * @param <K>
 * @param <V>
 */
public class MyHashMap<K, V> extends HashMap<K, V> {

	Map<V,K> reverseMap = new HashMap<V,K>();

	/**
	 * Method that inserts the Key, Value pair on the map and the inverse Value, Key on the reverse Map
	 * 
	 */
	@Override
	public V put(K key, V value) {
		if (reverseMap.get(value)!=null && reverseMap.get(value)!=key){
			throw new IllegalArgumentException("Cannot set an existing value to another assignment key");
		}		
		reverseMap.put(value, key);
		return super.put(key, value);
	}

	/**
	 * Method that gets the Key from reverse Map by the way of its value
	 * 
	 * @param value
	 * @return
	 */
	public K getKey(V value){
		return reverseMap.get(value);
	}
}
