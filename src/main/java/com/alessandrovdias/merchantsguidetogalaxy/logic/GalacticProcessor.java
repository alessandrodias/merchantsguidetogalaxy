package com.alessandrovdias.merchantsguidetogalaxy.logic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.alessandrovdias.merchantsguidetogalaxy.model.CreditsAssignmentSentence;
import com.alessandrovdias.merchantsguidetogalaxy.model.RomanAssignmentSentence;
import com.alessandrovdias.merchantsguidetogalaxy.model.interfaces.AssignmentSentence;
import com.alessandrovdias.merchantsguidetogalaxy.model.interfaces.QuerySentence;
import com.alessandrovdias.merchantsguidetogalaxy.model.interfaces.Sentence;
import com.alessandrovdias.merchantsguidetogalaxy.util.MyHashMap;

/**
 * Class responsible for process data inputs and integrate with Sentence and Parsers to convert
 * Galactic coinage to Roman terrestrial values and then to decimal Credits
 * 
 * @author Alessandro
 *
 */
public class GalacticProcessor {
	
	private static List<String> lines = new ArrayList<String>();
	private static Map<String, String> romanAssignments = new MyHashMap<String, String>();
	private static Map<String, String> creditsAssignments = new MyHashMap<String, String>();
	private static Map<String, String> tempAssignments; 
	
	/** 
	 * Method which separates lines of data, sending each line for further parsing
	 * 
	 * @param data
	 * @return
	 */
	public void processData(String data) {
		lines =  Arrays.asList(data.replaceAll("\r", "\n")
				   .replaceAll("\n{2,}", "\n")
				   .split("\n"));
		for (String dataLine : lines){
			processDataLine(dataLine);
		}
	}

	/**
	 * Method which calls the processing of a Sentence parsed by the SentenceFactory 
	 * 
	 * @param dataLine
	 */
	private static void processDataLine(String dataLine) {
		Sentence sentence = SentenceFactory.parseSentence(dataLine);
		
		if (sentence instanceof AssignmentSentence) {
			AssignmentSentence<?, ?> new_name = (AssignmentSentence<?, ?>) sentence;
			
			if (sentence instanceof CreditsAssignmentSentence){
				((CreditsAssignmentSentence) sentence).setRomanAssignments(romanAssignments);
			}
			tempAssignments = ((AssignmentSentence<String, String>) sentence).process(dataLine);	
			loadIntoAssignments(sentence);
		} else {
			if (sentence instanceof QuerySentence) {
				QuerySentence new_name = (QuerySentence) sentence;
				((QuerySentence) sentence).setRomanAssignments(romanAssignments);
				((QuerySentence) sentence).setCreditsAssignments(creditsAssignments);
				((QuerySentence) sentence).process(dataLine);
			}
		}
	}

	/**
	 * Method responsible for dynamically populate the assignment hashmaps,
	 * depending on what Sentence is being processed 
	 *  
	 * @param sentence
	 */
	private static void loadIntoAssignments(Sentence sentence) {
		MyHashMap<String, String> assignments = null;
		for (MyHashMap.Entry<String, String> entry:  tempAssignments.entrySet()){
			if (sentence instanceof CreditsAssignmentSentence){
				assignments = (MyHashMap<String, String>) creditsAssignments;
			}
			if (sentence instanceof RomanAssignmentSentence){
				assignments = (MyHashMap<String, String>) romanAssignments;
			}
			if (assignments.containsValue(entry.getValue()) && assignments.getKey(entry.getKey())!=null){
				throw new IllegalArgumentException("Cannot set an existing value to another assignment key");
			}
			assignments.put(entry.getKey(), entry.getValue());
		}
	}

	/** 
	 * Access method for lines attribute, mainly used for testing scenarios
	 * 
	 * @return List with lines
	 */
	public List<String> getLines() {
		return lines;
	}

	/** 
	 * Access method for Roman Assignments attribute, mainly used for testing scenarios
	 * 
	 * @return List with assignments
	 */
	public Map<String, String> getRomanAssignments() {
		return romanAssignments;
	}

	public static void setRomanAssignments(Map<String, String> romanAssignments) {
		GalacticProcessor.romanAssignments = romanAssignments;
	}

	public static void setCreditsAssignments(Map<String, String> creditsAssignments) {
		GalacticProcessor.creditsAssignments = creditsAssignments;
	}
}
