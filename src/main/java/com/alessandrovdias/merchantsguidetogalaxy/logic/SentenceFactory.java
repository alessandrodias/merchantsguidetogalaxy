package com.alessandrovdias.merchantsguidetogalaxy.logic;

import com.alessandrovdias.merchantsguidetogalaxy.model.CreditsAssignmentSentence;
import com.alessandrovdias.merchantsguidetogalaxy.model.CreditsAssignmentSentenceTest;
import com.alessandrovdias.merchantsguidetogalaxy.model.HowManyQuerySentence;
import com.alessandrovdias.merchantsguidetogalaxy.model.HowMuchQuerySentence;
import com.alessandrovdias.merchantsguidetogalaxy.model.NoIdeaSentence;
import com.alessandrovdias.merchantsguidetogalaxy.model.RomanAssignmentSentence;
import com.alessandrovdias.merchantsguidetogalaxy.model.interfaces.Sentence;

/**
 * Factory responsible for delivering Sentences
 * 
 * @author Alessandro
 *
 */
public class SentenceFactory {
	
    private static final String ROMAN_ASSIGNMENT_REGEX = "^[a-zA-Z]+( is )(I|V|X|L|C|D|M)$";
    private static final String CREDITS_ASSIGNMENT_REGEX = "^[a-zA-Z ]*[a-zA-Z]+( is )[0-9]+( Credits)$";
    private static final String HOW_MUCH_QUERY_REGEX = "^(how much is )[a-zA-Z ]+(\\?)$";
    private static final String HOW_MANY_QUERY_REGEX = "^(how many Credits is )[a-zA-Z ]+(\\?)$";

	/** 
	 * Method responsible for parsing a data line, extracting a Sentence 
	 * from it, according to what was received in data line
	 * 
	 * @param dataLine
	 * @return An instance of a subclass of Sentence
	 */
    public static Sentence parseSentence(String dataLine) {
		if (dataLine.matches(ROMAN_ASSIGNMENT_REGEX)){
			return new RomanAssignmentSentence();
		}
		if (dataLine.matches(CREDITS_ASSIGNMENT_REGEX)){
			return new CreditsAssignmentSentence();
		}
		if (dataLine.matches(HOW_MUCH_QUERY_REGEX)){
			return new HowMuchQuerySentence();
		}
		if (dataLine.matches(HOW_MANY_QUERY_REGEX)){
			return new HowManyQuerySentence();
		}		
		return new NoIdeaSentence();
	}
}
