package com.alessandrovdias.merchantsguidetogalaxy.logic;

import java.math.BigDecimal;
import java.util.Map;

import javafx.util.Pair;

/**
 * Class responsible for validating and parsing roman numbers, converting them to BigDecimal values 
 *  
 * @author Alessandro
 *
 */
public class RomanParser{
	private final static Pair[] ROMAN_VALUES = {
			new Pair<String, String>("IV", " 4 "),
			new Pair<String, String>("IX", " 9 "),
			new Pair<String, String>("XL", " 40 "),
			new Pair<String, String>("XC", " 90 "),
			new Pair<String, String>("CD", " 400 "),
			new Pair<String, String>("CM", " 900 "),
			new Pair<String, String>("I", " 1 "),  
			new Pair<String, String>("V", " 5 "),  
			new Pair<String, String>("X", " 10 "), 
			new Pair<String, String>("L", " 50 "), 	
			new Pair<String, String>("C", " 100 "),	
			new Pair<String, String>("D", " 500 "),	
			new Pair<String, String>("M", " 1000 ")
	};	
	private static final String SPACE_BETWEEN_LONG_VALUES = " ";
	
	/**
	 * Method which gets an assignment value, validates and converts it 
	 * from Roman to Decimal
	 * 
	 * @param assignmentValue
	 * @return BigDecimal number representing the Roman number given
	 */
	public BigDecimal parseValue(String assignmentValue) {
		BigDecimal finalValue = null;
		String romanValueToMount = assignmentValue;
		if (RomanValidator.validateRomanInput(romanValueToMount)){
			String valueMountedFromRomanValue = mountLongString(romanValueToMount);
			finalValue = addLongString(valueMountedFromRomanValue);
		}
		return finalValue;
	}	
		
	/**
	 * Method that converts Roman numbers in the String to their respective decimal values, 
	 * following the ROMAN_VALUES Pair for future addition
	 * 
	 * @param dataLine
	 * @return String with decimal numbers separated by spaces
	 */
	private String mountLongString(String dataLine){
		String dataLineCopied = dataLine;
		for (Pair<String, String> romanValue : ROMAN_VALUES) {
			dataLineCopied = dataLineCopied.replaceAll(romanValue.getKey(), romanValue.getValue());
		}
		return dataLineCopied;
	}
	
	/**
	 * Method that adds each Decimal number to each other, resulting in the final Roman number converted to
	 * BigDecimal 
	 * 
	 * @param longValueMountedFromRomanValue
	 * @return
	 */
	private BigDecimal addLongString(String longValueMountedFromRomanValue) {
		BigDecimal addedNumber = new BigDecimal(0);
		for (String element : longValueMountedFromRomanValue.trim().replaceAll("  ", " ").split(SPACE_BETWEEN_LONG_VALUES)) {
			addedNumber = addedNumber.add(new BigDecimal(element.trim()));
		}
		return addedNumber;
	}
	
	/**
	 * Method responsible for mounting the final Roman numeral multiplying the Metal and returning the multiplier to
	 * help discover Metal value in Assignment 
	 * 
	 * @param romanAssignedPart
	 * @return
	 */
	public BigDecimal getDecimalFromRomanAssignedPart(String[] romanAssignedPart,  Map<String, String> romanAssignments) {
		String romanNumeral = "";
		for (String key : romanAssignedPart) {
			String numeralToConcatenate = romanAssignments.get(key);
			if (numeralToConcatenate==null){
				throw new IllegalArgumentException("Invalid character in the data line, could not find in romanAssignments");
			}
			romanNumeral = romanNumeral + numeralToConcatenate ;
		}
		return parseValue(romanNumeral);
	}
}
