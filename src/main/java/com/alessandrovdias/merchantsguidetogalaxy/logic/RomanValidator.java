package com.alessandrovdias.merchantsguidetogalaxy.logic;

/**
 * Class that is responsible for validating if a given Roman string is really
 * a acceptable Roman symbol 
 * 
 * @author Alessandro
 *
 */
public class RomanValidator {
	
	private static final String ROMAN_REGEX_VALIDATOR = "^M{0,3}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$";

	/**
	 * Method that uses a Regex to validate if the Roman number is correct 
	 * according to definitions
	 * 
	 * @param dataLine
	 */
	public static boolean validateRomanInput(String dataLine) {
		if (!dataLine.matches(ROMAN_REGEX_VALIDATOR)) {
			throw new IllegalArgumentException("Invalid roman character in the data line: " + dataLine);
		}
		return true;
	}
}
