package com.alessandrovdias.merchantsguidetogalaxy.model;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import com.alessandrovdias.merchantsguidetogalaxy.logic.RomanParser;
import com.alessandrovdias.merchantsguidetogalaxy.model.interfaces.QuerySentence;

/**
 * Class that is responsible for processing and validating sentences 
 * of How Many Credits queries
 * 
 * @author Alessandro
 *
 */
public class HowManyQuerySentence implements QuerySentence {
	
	private static final java.lang.String SPACE = " ";
    private static final String HOW_MANY_TEXT = "how many credits is ";
    private static final String QUESTION_MARK = " ?";
	
	private Map<String, String> creditsAssignments = new HashMap<String, String>();
	private Map<String, String> romanAssignments = new HashMap<String, String>();
	private RomanParser romanParser = new RomanParser();

	/**
	 * Method which process a HowManyQuerySentence line, separating Roman and 
	 * Credits values and then evaluates and write the results on screen
	 *
	 * @param dataLine
	 * @return
	 */
	@Override
	public void process(String dataLine) {
		String galacticValues = parseGalacticValuesFromDataLine(dataLine);
		String[] romanValues = extractValuesFromGalacticValues(galacticValues, romanAssignments);
		String[] creditsValues = extractValuesFromGalacticValues(galacticValues, creditsAssignments);
        BigDecimal totalRomanValue = romanParser.getDecimalFromRomanAssignedPart(romanValues, romanAssignments);
        BigDecimal totalCreditsValue = sumCreditsValue(creditsValues, creditsAssignments);
        writeResult(galacticValues, totalRomanValue, totalCreditsValue);
	}

	/**
	 * Method responsible for writing on default system out
	 * 
	 * @param galacticValues
	 * @param totalRomanValue
	 * @param totalCreditsValue
	 */
	private void writeResult(String galacticValues, BigDecimal totalRomanValue, BigDecimal totalCreditsValue) {
		BigDecimal result = (totalRomanValue.multiply(totalCreditsValue)).stripTrailingZeros();
        System.out.println(galacticValues + " is " + result.toPlainString() + " Credits");
	}

	/**
	 * Method that sum the results of Credits passed to it, consulting in the map passed to it 
	 * 
	 * @param creditsValues
	 * @param assignments
	 * @return
	 */	
	private BigDecimal sumCreditsValue(String[] creditsValues, Map<String, String> assignments) {
		BigDecimal sum = new BigDecimal(0);
		for(String creditValue: creditsValues){
			sum = sum.add(new BigDecimal(assignments.get(creditValue)));
		}
		return sum;
	}

	/**
	 * Method that obtain values of a string based on the map passed to it
	 * 
	 * @param galacticValues
	 * @param assignments
	 * @return
	 */
	private String[] extractValuesFromGalacticValues(String galacticValues, Map<String, String> assignments ) {
		String values = "";
		for(String galacticValue: galacticValues.split(SPACE)){
			if (assignments.containsKey(galacticValue)){
				values = values + galacticValue + SPACE;
			}
		}
		return values.split(SPACE);
	}

	@Override
	public void setRomanAssignments(Map<String, String> romanAssignments) {
		this.romanAssignments = romanAssignments;
	}

	@Override
	public void setCreditsAssignments(Map<String, String> creditsAssignments) {
		this.creditsAssignments = creditsAssignments;
	}
	
	/**
	 * Method responsible for parse the sentence, removing predefined prefix and
	 * suffix 
	 * 
	 * @param dataLine
	 * @return
	 */
	private String parseGalacticValuesFromDataLine(String dataLine) {
		return dataLine.substring(HOW_MANY_TEXT.length(), dataLine.length() - QUESTION_MARK.length());
	}

}
