package com.alessandrovdias.merchantsguidetogalaxy.model.interfaces;

import java.util.Map;

public interface QuerySentence extends Sentence{
	
	public void process(String dataLine);
	
	public void setRomanAssignments(Map<String, String> romanAssignments);

	public void setCreditsAssignments(Map<String, String> creditsAssignments);

}
