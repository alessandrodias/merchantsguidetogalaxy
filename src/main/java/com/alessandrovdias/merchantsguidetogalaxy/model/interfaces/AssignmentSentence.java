package com.alessandrovdias.merchantsguidetogalaxy.model.interfaces;

import java.util.Map;

public interface AssignmentSentence<K, V> extends Sentence {
	
	public Map<K, V> process(String dataLine);

}
