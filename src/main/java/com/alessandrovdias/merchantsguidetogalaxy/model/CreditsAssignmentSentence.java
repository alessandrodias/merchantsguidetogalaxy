package com.alessandrovdias.merchantsguidetogalaxy.model;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import com.alessandrovdias.merchantsguidetogalaxy.logic.RomanParser;
import com.alessandrovdias.merchantsguidetogalaxy.model.interfaces.AssignmentSentence;

/**
 * Class that is responsible for processing and validating sentences of 
 * Credits values Assignment 
 * 
 * @author Alessandro
 *
 */
public class CreditsAssignmentSentence implements AssignmentSentence<String, String> {

	private static final String SPACE = " ";
	private static final String ASSIGNMENT_OPERATOR = "is";
	private Map<String, String> creditsAssignments = new HashMap<String, String>();
	private Map<String, String> romanAssignments = new HashMap<String, String>();
	private RomanParser romanParser = new RomanParser();

	/**
	 * Method which process a CreditsAssignmentSentence line, separating operators from 
	 * Credits value assigned, resulting in a Map with the String Key, BigDecimal value pair
	 *
	 * @param dataLine
	 * @return
	 */
	@Override
	public Map<String, String> process(String dataLine) {
		String[] assignment = validateDataLine(dataLine);
		int spacePosition = findIsInAssignment(assignment);
		
		BigDecimal value = new BigDecimal(assignment[spacePosition+1]);
		BigDecimal multiplier = romanParser.getDecimalFromRomanAssignedPart(Arrays.copyOfRange(assignment, 0, spacePosition-1), romanAssignments);

		creditsAssignments.put(assignment[spacePosition-1], String.valueOf(value.divide(multiplier)));
		return creditsAssignments;
	}
	
	/**
	 * Method that validates if the dataLine is respecting the expected
	 * format 
	 * 
	 * @param dataLine
	 * @return
	 */
	private String[] validateDataLine(String dataLine) {
		if (dataLine==null || !dataLine.contains(SPACE)){
			throw new IllegalArgumentException("Invalid sentence in the data line");
		}
		String[] assignment = dataLine.split(SPACE);
		if (assignment.length<3) {
			throw new IllegalArgumentException("Invalid sentence in the data line");
		}
		return assignment;
	}

	/**
	 * Method responsible for locating the IS operator in the assignment
	 * 
	 * @param assignment
	 * @return
	 */
	private int findIsInAssignment(String[] assignment) {
		int isPosition = Arrays.asList(assignment).indexOf(ASSIGNMENT_OPERATOR);
		if (isPosition==-1){
			throw new IllegalArgumentException("Invalid sentence in the data line");
		}
		return isPosition;
	}

	/**
	 * Method that is used to set the Roman assignments already defined for using in Credits Assignment 
	 * discovery of Metal values
	 * 
	 * @param assignments
	 */
	public void setRomanAssignments(Map<String, String> assignments) {
		this.romanAssignments = assignments;
	}
}
