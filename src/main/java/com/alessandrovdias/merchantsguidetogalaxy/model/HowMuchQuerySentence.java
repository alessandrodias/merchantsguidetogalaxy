package com.alessandrovdias.merchantsguidetogalaxy.model;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import com.alessandrovdias.merchantsguidetogalaxy.logic.RomanParser;
import com.alessandrovdias.merchantsguidetogalaxy.model.interfaces.QuerySentence;

/**
 * Class that is responsible for processing and validating sentences 
 * of How Much queries
 * 
 * @author Alessandro
 *
 */
public class HowMuchQuerySentence implements QuerySentence {

    private static final java.lang.String SPACE = " ";
    private static final String HOW_MUCH_TEXT = "how much is ";
    private static final String QUESTION_MARK = " ?";
	
	private Map<String, String> creditsAssignments = new HashMap<String, String>();
	private Map<String, String> romanAssignments = new HashMap<String, String>();
	private RomanParser romanParser = new RomanParser();

	/**
	 * Method which process a HowMuchQuerySentence line, evaluating Roman 
	 * values and then write the results on screen
	 *
	 * @param dataLine
	 * @return
	 */
	@Override
	public void process(String dataLine) {
		String galacticValues = parseGalacticValuesFromDataLine(dataLine);
        BigDecimal finalValue = romanParser.getDecimalFromRomanAssignedPart(galacticValues.split(SPACE), romanAssignments);
        writeResult(galacticValues, finalValue);
	}

	/**
	 * Method responsible for writing on default system out
	 * 
	 * @param galacticValues
	 * @param finalValue
	 */
	private void writeResult(String galacticValues, BigDecimal finalValue) {
        System.out.println(galacticValues + " is " + finalValue);		
	}

	/**
	 * Method responsible for parse the sentence, removing predefined prefix and
	 * suffix 
	 * 
	 * @param dataLine
	 * @return
	 */
	private String parseGalacticValuesFromDataLine(String dataLine) {
		return dataLine.substring(HOW_MUCH_TEXT.length(), dataLine.length() - QUESTION_MARK.length());
	}

	public Map<String, String> getCreditsAssignments() {
		return creditsAssignments;
	}

	public void setCreditsAssignments(Map<String, String> creditsAssignments) {
		this.creditsAssignments = creditsAssignments;
	}

	public Map<String, String> getRomanAssignments() {
		return romanAssignments;
	}

	public void setRomanAssignments(Map<String, String> romanAssignments) {
		this.romanAssignments = romanAssignments;
	}

}
