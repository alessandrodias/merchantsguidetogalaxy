package com.alessandrovdias.merchantsguidetogalaxy.model;

import java.util.Map;

import com.alessandrovdias.merchantsguidetogalaxy.logic.RomanValidator;
import com.alessandrovdias.merchantsguidetogalaxy.model.interfaces.AssignmentSentence;
import com.alessandrovdias.merchantsguidetogalaxy.util.MyHashMap;

/**
 * Class that is responsible for processing and validating sentences of 
 * Roman number Assignment 
 * 
 * @author Alessandro
 *
 */
public class RomanAssignmentSentence implements AssignmentSentence<String, String> {

	private static final String ASSIGNMENT_OPERATOR = " is ";

	private Map<String, String> romanSymbols = new MyHashMap<String, String>();

	/**
	 * Method which process a RomanSentence line, separating operator from 
	 * value assigned, resulting in a Map with the String Key, BigDecimal value pair
	 *
	 * @param dataLine
	 * @return
	 */
	public Map<String, String> process(String dataLine) {
		if (dataLine==null || !dataLine.contains(ASSIGNMENT_OPERATOR)){
			throw new IllegalArgumentException("Invalid sentence in the data line");
		}
		String[] assignment = dataLine.split(ASSIGNMENT_OPERATOR);
		if (assignment.length!=2) {
			throw new IllegalArgumentException("Invalid sentence in the data line");
		}
		if (RomanValidator.validateRomanInput(assignment[1])){
			romanSymbols.put(assignment[0], assignment[1]);
		}
		return romanSymbols;		
	}
}
