package com.alessandrovdias.merchantsguidetogalaxy.model;

import java.util.Map;

import com.alessandrovdias.merchantsguidetogalaxy.model.interfaces.QuerySentence;

public class NoIdeaSentence implements QuerySentence {
    private static final String MESSAGE = "I have no idea what you are talking about";

	@Override
	public void process(String dataLine) {
		System.out.println(MESSAGE);		
	}

	@Override
	public void setRomanAssignments(Map<String, String> romanAssignments) {
				
	}

	@Override
	public void setCreditsAssignments(Map<String, String> creditsAssignments) {
				
	}
	
}
